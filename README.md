## Formação em Analista de BI

Esse repositório é parte do curso de Formação de Analista de BI. É um exemplo de um projeto de implementação de BI completo, desde a extração dos dados da fonte original, modelagem em formato star-schema até a visualização dos dados finais em uma ferramenta de BI.

### Requerimentos:

- Python 3+
- Docker
- PostgresSQL 10+
- psql
- Dbt
- Metabase
- dBeaver

> **NOTA:** Os examples serão feitos utilizando o ambiente LINUX. Embora a maioria dessas ferramentas possua compatibilidade com o WINDOWS, pode ser necessário alguns ajustes.


### Instalando o PostgreSQL

 A forma mais fácil de instalar o banco de dados PostgreSQL é através da imagem Docker. Ao rodar o comando abaixo nós criamos uma instância do banco de dados na porta padrão 5432:

``` bash
 sudo docker run -it --name aula_bi -p 5432:5432  -d  postgres:latest
```

### Carregando os dados de exemplo

Este projeto contém três bancos de dado de exemplo:

- [AdventureWorks](./adventureworks): Banco de dados de uma multinacional fictícia de Bicicletas
- [Chinook](./chinook-1.4): Banco de dados de um serviço de streaming de músicas.
- [Dellstore](./dellstore2-normal-1.0): Banco de dados de um ecommerce

Na pasta de cada um há um script que carrega os dados no banco de dados. Neste exemplo vamos utilizar o banco de dados da Chinook:

``` bash
 # Conectar no banco de dados
 psql -U postgres -p 5432 -h localhost

 # Criar o database
 CREATE DATABASE chinook;

 # Ou direto do terminal
 createdb -E UTF8 chinook -U postgres -p 5432 -h localhost

 # Subir os dados para o banco
psql -f chinook-1.4/Chinook_PostgreSql_utf8.sql -d chinook  -U postgres -p 5432 -h localhost  
```

Se tudo  funcionou corretamente, temos um banco de dados transacional dentro do banco de dados.

``` bash
 # Listar as tabelas no banco
psql -U postgres -p 5432 -h localhost -d chinook

\d+


                          List of relations
 Schema |     Name      | Type  |  Owner   |    Size    | Description 
--------+---------------+-------+----------+------------+-------------
 public | Album         | table | postgres | 48 kB      | 
 public | Artist        | table | postgres | 40 kB      | 
 public | Customer      | table | postgres | 40 kB      | 
 public | Employee      | table | postgres | 8192 bytes | 
 public | Genre         | table | postgres | 8192 bytes | 
 public | Invoice       | table | postgres | 72 kB      | 
 public | InvoiceLine   | table | postgres | 144 kB     | 
 public | MediaType     | table | postgres | 8192 bytes | 
 public | Playlist      | table | postgres | 8192 bytes | 
 public | PlaylistTrack | table | postgres | 336 kB     | 
 public | Track         | table | postgres | 384 kB     |
```

### Explorando o modelo de dados 

A primeira etapa da implementação do BI é entender o modelo de dados dos dados originais. Isso quer dizer entender as relações entre as tabelas, foreign keys, tipos, etc.

![](./chinook-1.4/datamodel.png)

Nem sempre é possível gerar esse modelo automaticamente, mas algumas ferramentas como o SchemaSpy ou dBeaver podem facilitar nesse processo. Cabe lembrar que bancos de dados transacionais não raramente possuem milhares de tabelas normalizadas sem um dicionário de dados de fácil acesso.

#### Utilizando o dbeaver

Dbeaver é um cliente SQL visual que permite conectar e explorar diversos bancos de dados. Para instalá-lo:

``` bash
sudo snap install dbeaver-ce
dbeaver-ce
```
> **NOTA:** Pode ser necessário instalar drivers adicionais.

Para visualizar o modelo de dados das tabelas, utilizamos o *E-R diagram* disponível ao selecionar um objeto do banco como tabelas ou schemas.

![](dbeaver_erdiagram.png)

### Criando o Data Warehouse

Para transformar as tabelas do formato transacional (OLTP) para o *star schema* de um Data Warehouse, utilizamos a ferramenta **dbt**.

#### Instalando o Dbt

Para instalar o dbt precisamos instalar algumas dependências

``` bash
sudo apt-get install git libpq-dev python-dev
```

E depois instalar usando `pip`:

``` bash
pip install dbt
```

Ou seguir as [instruções do site](https://docs.getdbt.com/docs/installation).

#### Configurando e Modelando

[a ser feito]

Neste momento vamos utilizar o material disponível no [indicium-etl](https://bitbucket.org/indiciumtech/indicium-etl/src/619453b41ee73abdecc49715e984ede45e6d5ba0/transform/dbt-pipelines/README.md)


### Visualizando no BI

#### Metabase

Metabase é uma ferramenta de BI *open-source* que pode ser utilizada em diferentes projetos:


```bash
docker run -d -p 3000:3000 -v ~/metabase-data:/metabase-data --network="host" -e "MB_DB_FILE=/metabase-data/metabase.db" --name metabase metabase/metabase
```